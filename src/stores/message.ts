import { ref } from 'vue'
import { defineStore } from 'pinia'

export const useMessageStore = defineStore('message', () => {
  const snacbar = ref(false)
  const text = ref('')
  const showMessage = function (msg: string) {
    text.value = msg
    snacbar.value = true
  }
  return { showMessage, snacbar, text }
})
